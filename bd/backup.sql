CREATE SCHEMA bd_ciclon;

CREATE TABLE bd_ciclon.tbl_contacto ( 
	id_contacto          bigint UNSIGNED NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	nombres              varchar(50)      ,
	apellido_paterno     varchar(50)      ,
	apellido_materno     varchar(50)      ,
	telefono             varchar(9)      ,
	celular              varchar(9)      ,
	email                varchar(30)      ,
	asunto               text      ,
	mensaje              text      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	fecha_reacion        timestamp   DEFAULT CURRENT_TIMESTAMP   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_list ( 
	id_list              bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	nombre               varchar(50)      
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_list_detail ( 
	id_list_detail       bigint  NOT NULL    ,
	numero               int  NOT NULL    ,
	nombre               varchar(50)      ,
	imagen               text      ,
	CONSTRAINT pk_tbl_list_detail_id_list_detail PRIMARY KEY ( id_list_detail, numero )
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_objeto_tipo ( 
	id_objeto_tipo       bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	nombre               varchar(255)      ,
	descripcion          text      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_persona ( 
	id_persona           bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	dni                  char(8)  NOT NULL    ,
	nombres              varchar(50)  NOT NULL    ,
	apellido_paterno     varchar(50)  NOT NULL    ,
	apellido_materno     varchar(50)  NOT NULL    ,
	telefono             varchar(9)      ,
	celular              varchar(9)      ,
	email                varchar(30)      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	fecha_creacion       timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_proforma ( 
	id_proforma          bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	nombres              varchar(50)      ,
	apellido_paterno     varchar(50)      ,
	apellido_materno     varchar(50)      ,
	telefono             varchar(9)      ,
	celular              varchar(9)      ,
	email                varchar(30)      ,
	dirección_origen     text      ,
	direccion_origen_referencia text      ,
	dirección_origen_piso int      ,
	direccion_destino    text      ,
	direccion_destino_referencia text      ,
	direccion_destino_piso int      ,
	fecha                date      ,
	hora                 time      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	fecha_reacion        timestamp   DEFAULT CURRENT_TIMESTAMP   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_usuario ( 
	id_usuario           bigint  NOT NULL    PRIMARY KEY,
	password             char(32)  NOT NULL    ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	fecha_reacion        timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_vehiculo ( 
	id_vehiculo          bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	nombre               varchar(255)      ,
	imagen               text      ,
	placa                varchar(10)      ,
	ancho                decimal      ,
	alto                 decimal      ,
	largo                decimal      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_mudanza ( 
	id_mudanza           bigint  NOT NULL    PRIMARY KEY,
	id_proforma          bigint      ,
	id_vehiculo          bigint      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_objeto ( 
	id_objeto            bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	nombre               varchar(255)      ,
	imagen               text      ,
	ancho                decimal      ,
	alto                 decimal      ,
	largo                decimal      ,
	id_objeto_tipo       bigint      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   
 ) engine=InnoDB;

CREATE TABLE bd_ciclon.tbl_proforma_detalle ( 
	id_proforma_detalle  bigint  NOT NULL    ,
	numero               int  NOT NULL    ,
	id_objeto            bigint      ,
	cantidad             int  NOT NULL DEFAULT 1   ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	CONSTRAINT pk_tbl_mudanza_detalle_id_mudanza_detalle PRIMARY KEY ( id_proforma_detalle, numero )
 ) engine=InnoDB;

ALTER TABLE bd_ciclon.tbl_list_detail ADD CONSTRAINT fk_tbl_list_detail_tbl_list FOREIGN KEY ( id_list_detail ) REFERENCES bd_ciclon.tbl_list( id_list ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE bd_ciclon.tbl_mudanza ADD CONSTRAINT fk_tbl_mudanza_tbl_proforma FOREIGN KEY ( id_proforma ) REFERENCES bd_ciclon.tbl_proforma( id_proforma ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE bd_ciclon.tbl_mudanza ADD CONSTRAINT fk_tbl_mudanza_tbl_vehiculo FOREIGN KEY ( id_vehiculo ) REFERENCES bd_ciclon.tbl_vehiculo( id_vehiculo ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE bd_ciclon.tbl_objeto ADD CONSTRAINT fk_tbl_objeto_tbl_objeto_tipo FOREIGN KEY ( id_objeto_tipo ) REFERENCES bd_ciclon.tbl_objeto_tipo( id_objeto_tipo ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE bd_ciclon.tbl_proforma_detalle ADD CONSTRAINT fk_tbl_mudanza_detalle_tbl_mudanza FOREIGN KEY ( id_proforma_detalle ) REFERENCES bd_ciclon.tbl_proforma( id_proforma ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE bd_ciclon.tbl_proforma_detalle ADD CONSTRAINT fk_tbl_mudanza_detalle_tbl_objeto FOREIGN KEY ( id_objeto ) REFERENCES bd_ciclon.tbl_objeto( id_objeto ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE bd_ciclon.tbl_usuario ADD CONSTRAINT fk_tbl_usuario_tbl_persona FOREIGN KEY ( id_usuario ) REFERENCES bd_ciclon.tbl_persona( id_persona ) ON DELETE NO ACTION ON UPDATE NO ACTION;

